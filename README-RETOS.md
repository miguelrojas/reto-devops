### Reto 1. Dockerize la aplicación

1. Descarga de respositorio

```bash
$ https://gitlab.com/miguelrojas/reto-devops.git

Cloning into 'reto-devops'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (3/3), done.
$ cd ./reto-devops

```

2. construcción imagen



```bash
$ docker login

$ docker build -t {userdocker}/nodereto-slim:v1 

docker run --name node-slim -p 3000:3000 -d {userdocker}/nodereto-slim:v1

$ curl -s localhost:3000/ | jq
{
  "msg": "ApiRest prueba"
}
$ curl -s localhost:3000/public | jq
{
  "public_token": "12837asd98a7sasd97a9sd7"
}
$ curl -s localhost:3000/private | jq
{
  "private_token": "TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="
}
```

3. Docker compose



```bash
$ docker compose build

$ docker compose up -d 

curl --insecure https://172.18.0.3 | jq

curl --insecure https://172.18.0.3/public | jq

curl --insecure -u "root:root" https://172.18.0.3/private | jq

```