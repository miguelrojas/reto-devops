FROM node:14.15.4-slim


ENV PROYECTO=/usr/src/reto
ENV PORT=3000

RUN mkdir -p ${PROYECTO} \
    && chown node:node ${PROYECTO}

WORKDIR /usr/src/reto 

COPY package*.json ./

RUN npm install \
    &&  usermod -d ${PROYECTO} node

COPY . ./

RUN chown -R node: ../*

USER node 

EXPOSE ${PORT}

CMD [ "node", "index.js" ]